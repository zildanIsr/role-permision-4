const express = require('express') //inisiasi variable yang berisi express
const router = express.Router() // inisiasi variable yang berisi fungsi router express
const { list, create, update, destroy, findById } = require('../controllers/categoryController') // inisiasi object controller
const validate = require('../middleware/validate')
const { createCategoryRules } = require('../validators/rule')
const checkToken = require('../middleware/checkToken')
const checkroles = require('../middleware/hasRoles')

router.get('/list', checkToken, checkroles, list) // route untuk endpoint list
router.post('/find-by-id', checkToken, checkroles, findById) // route untuk endpoint list
router.post('/create', checkToken, checkroles, validate(createCategoryRules), create) // route untuk endpoint create
router.put('/update', checkToken,checkroles, update) // route untuk endpoint update
router.delete('/destroy', checkToken, checkroles, destroy) // route untuk endpoint destroy

module.exports = router // export fungsi router agar module lain bisa membaca file ini